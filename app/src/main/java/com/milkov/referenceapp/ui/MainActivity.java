package com.milkov.referenceapp.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.milkov.referenceapp.R;
import com.milkov.referenceapp.communication.CustomRetrofitCallback;
import com.milkov.referenceapp.communication.RetrofitRestClient;
import com.milkov.referenceapp.communication.model.TestModel;
import com.milkov.referenceapp.communication.service.ApiService;

import java.util.List;

import retrofit.Call;
import retrofit.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RetrofitRestClient retrofitRestClient = new RetrofitRestClient();
        ApiService apiService = retrofitRestClient.getApiService();

        Call<List<TestModel>> call = apiService.getPosts();
        call.enqueue(new CustomRetrofitCallback<List<TestModel>>() {
            @Override
            public void on200(Response<List<TestModel>> response) {
                Log.d("===", "success");
            }

            @Override
            public void on400(Response<List<TestModel>> response) {
            }

            @Override
            public void on401(Response<List<TestModel>> response) {
            }

            @Override
            public void on403(Response<List<TestModel>> response) {
            }

            @Override
            public void on404(Response<List<TestModel>> response) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
