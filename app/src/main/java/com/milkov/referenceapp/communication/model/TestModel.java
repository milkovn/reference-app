package com.milkov.referenceapp.communication.model;

public class TestModel {

    private long mUserId;

    private long mId;

    private String mTitle;

    private String mBody;

    public TestModel(long userId, long id, String title, String body) {
        mUserId = userId;
        mId = id;
        mTitle = title;
        mBody = body;
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String mBody) {
        this.mBody = mBody;
    }
}
