package com.milkov.referenceapp.communication;

import com.milkov.referenceapp.communication.service.ApiService;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RetrofitRestClient {

    private ApiService mApiService;

    public RetrofitRestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = retrofit.create(ApiService.class);
    }

    public ApiService getApiService() {
        return mApiService;
    }
}
