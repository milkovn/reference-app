package com.milkov.referenceapp.communication.service;

import com.milkov.referenceapp.communication.model.TestModel;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

public interface ApiService {

    @GET("posts")
    Call<List<TestModel>> getPosts();
}
