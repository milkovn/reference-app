package com.milkov.referenceapp.communication;

import android.util.Log;

import retrofit.Callback;
import retrofit.Response;

public abstract class CustomRetrofitCallback<T> implements Callback<T> {

    public abstract void on200(Response<T> response);

    public abstract void on400(Response<T> response);

    public abstract void on401(Response<T> response);

    public abstract void on403(Response<T> response);

    public abstract void on404(Response<T> response);

    @Override
    public void onResponse(Response<T> response) {
        if (response.isSuccess()) {
            on200(response);
        } else {
            int httpCode = response.raw().code();
            switch (httpCode) {
                case 400:
                    on400(response);
                    break;
                case 401:
                    on401(response);
                    break;
                case 403:
                    on403(response);
                    break;
                case 404:
                    on404(response);
                    break;
                default:
                    Log.d("===", "Unsupported http code occurred:" + httpCode);
            }
        }
    }

    @Override
    public void onFailure(Throwable t) {
        Log.e("===", "Exception occurred while making http request: " + t.getMessage());
    }
}
